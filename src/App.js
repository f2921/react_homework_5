import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import "./App.css";
import Router from "./components/Routes";
import { getData, loadData } from "./Features/Products/productslice";
import ModalData from "./components/ModalData/ModalData";
import {
  showCartModal,
  addToCart,
  editCart,
  getDeleteCartModal,
  showDeleteModal,
  deleteFromCart,
  editCheckout,
  getCartModal,
  returnData,
  addCheckoutProduct,
  returnCheckoutData,
} from "./Features/Carts/cartslice";
import {
  addToFav,
  returnFav,
  editFav,
  deleteFromFav,
} from "./Features/Favorites/favorites";

function App() {
  const { products } = useSelector(getData);
  const cartModal = useSelector(getCartModal);
  const deleteCartModal = useSelector(getDeleteCartModal);
  const cartData = useSelector(returnData);
  const favoriteData = useSelector(returnFav);
  const checkoutData = useSelector(returnCheckoutData);
  const dptch = useDispatch();
  const [checkItem, setCheckitem] = useState([]);

  useEffect(function () {
    dptch(loadData());
    if (localStorage.getItem("cartData")) {
      dptch(editCart(JSON.parse(localStorage.cartData)));
    }

    if (localStorage.getItem("favData")) {
      dptch(editFav(JSON.parse(localStorage.favData)));
    }

    if (localStorage.getItem("checkoutData")) {
      dptch(editCheckout(JSON.parse(localStorage.checkoutData)));
    }
  }, []);

  const checkItemData = (product) => {
    setCheckitem(product);
  };

  const openAddToCartModal = () => {
    dptch(showCartModal());
  };

  const openDeleteCartModal = () => {
    dptch(showDeleteModal());
  };

  const addToCartData = (product) => {
    dptch(addToCart(product));
  };

  const addToFavorite = (product) => {
    dptch(addToFav(product));
  };

  const deleteFavorite = (product) => {
    dptch(deleteFromFav(product));
  };

  const deleteFromCartData = (product) => {
    dptch(deleteFromCart(product));
  };

  const addcheckoutData = (product) => {
    const newData = cartData ? [...cartData, product] : product;
    dptch(addCheckoutProduct(newData));
  };
  console.log(1111, checkoutData);
  return (
    <div className="App">
      <Router
        products={products}
        openAddToCartModal={openAddToCartModal}
        checkItemData={checkItemData}
        cartData={cartData}
        favData={favoriteData}
        addToFav={addToFavorite}
        deleteFavorite={deleteFavorite}
        showDeleteModal={openDeleteCartModal}
        addcheckoutData={addcheckoutData}
      />
      {cartModal && (
        <ModalData
          modalHeader="Do you want to add?"
          modalTitle="Cart Modal"
          modalText="Happy schopping!!!"
          background="blue"
          colorButtonOk="orange"
          colorButtonCancel="red"
          buttonName="Add"
          showAddToCartModal={openAddToCartModal}
          action={addToCartData}
          checkItem={checkItem}
        />
      )}
      {deleteCartModal && (
        <ModalData
          modalHeader="Do you want to delete?"
          modalTitle="Cart Delete Modal"
          modalText="Happy schopping!!!"
          background="blue"
          colorButtonOk="orange"
          colorButtonCancel="red"
          buttonName="Delete"
          showAddToCartModal={openDeleteCartModal}
          action={deleteFromCartData}
          checkItem={checkItem}
        />
      )}
    </div>
  );
}

export default App;
