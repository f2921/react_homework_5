export const favData = {
  favoriteProduct: [],
};

export function favReducer(state = {}, action) {
  if (action.type == "addToFav") {
    return { ...state, favoriteProduct: action.payload.favoriteProduct };
  }
  if (action.type == "deleteFromFav") {
    return { ...state, favoriteProduct: action.payload.favoriteProduct };
  }

  return state;
}

export function returnFav(state){
    return state.favdata.favoriteProduct;
}

export function editFav(data) {
  return {
    type: "addToFav",
    payload: {
      favoriteProduct: data,
    },
  };
}

export function deleteFav(data) {
    return {
      type: "deleteFromFav",
      payload: {
        favoriteProduct: data,
      },
    };
  }

export function addToFav(product) {
  return (dispatch, getstate) => {
    const favData = getstate().favdata.favoriteProduct;
    const newData = favData ? [...favData, product] : product;
    localStorage.favData = JSON.stringify(newData);
    return dispatch(editFav(newData));
  };
}

export function deleteFromFav(product) {
    return (dispatch, getstate) => {
      const newData = getstate().favdata.favoriteProduct.filter((item) => item.id !== product.id);
      localStorage.favData = JSON.stringify(newData);
      return dispatch(deleteFav(newData));
    };
  }
  