import {createStore,combineReducers, applyMiddleware} from "redux";
import { productData,productReducer } from "../Features/Products/productslice";
import thunk from "redux-thunk";
import { cartData, CartReducer } from "../Features/Carts/cartslice";
import { favData, favReducer } from "../Features/Favorites/favorites";

const store = createStore(combineReducers({
    products:productReducer,
    cartdata:CartReducer,
    favdata:favReducer,
}),{
    products:productData,
    cartdata:cartData,
    favdata:favData,
},applyMiddleware(thunk))

 export default store;