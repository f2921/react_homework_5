import { Formik, Field } from "formik";
import * as yup from "yup";
import "./formochka.scss";

const initialValid = yup.object().shape({
  firstName: yup
    .string()
    .required("Field is required")
    .min(5, "Field must be min 5 charsets"),
  lastName: yup
    .string()
    .required("Field is required")
    .min(5, "Field must be min 5 charsets"),
  age: yup
    .number("Field must be number")
    .required("Field is required")
    .positive("Must be positive number")
    .moreThan(18, "Age must be more than 18"),
  deliveryAdress: yup.string().required("Field is required"),
  phoneNumber: yup.number("Field must be number").required("Field is required"),
});

export default function Formochka({ addcheckoutData }) {
  const formikData = {
    firstName: "",
    lastName: "",
    age: "",
    deliveryAdress: "",
    phoneNumber: "",
  };
  return (
    <div className="form-section">
      <h1>Checkout</h1>
      <Formik
        initialValues={formikData}
        validationSchema={initialValid}
        onSubmit={addcheckoutData}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
        }) => (
          <form className="checkoutData-form" onSubmit={handleSubmit}>
            <Field
              type={`text`}
              name={`firstName`}
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.firstName}
              className="checkout-field"
              placeholder={`First Name`}
            />
            {errors.firstName && touched.firstName && (
              <p className="error-msg">{errors.firstName}</p>
            )}
            <Field
              type={`text`}
              name={`lastName`}
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.lastName}
              className="checkout-field"
              placeholder={`Last Name`}
            />
            {errors.lastName && touched.lastName && (
              <p className="error-msg">{errors.lastName}</p>
            )}
            <Field
              type={`number`}
              name={`age`}
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.age}
              className="checkout-field"
              placeholder={`Age`}
            />
            {errors.age && touched.age && (
              <p className="error-msg">{errors.age}</p>
            )}
            <Field
              type={`text`}
              name={`deliveryAdress`}
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.deliveryAdress}
              className="checkout-field"
              placeholder={`Adress`}
            />
            {errors.deliveryAdress && touched.deliveryAdress && (
              <p className="error-msg">{errors.deliveryAdress}</p>
            )}
            <Field
              type={`number`}
              name={`phoneNumber`}
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.phoneNumber}
              className="checkout-field"
              placeholder={`Phone number`}
            />
            {errors.phoneNumber && touched.phoneNumber && (
              <p className="error-msg">{errors.phoneNumber}</p>
            )}

            <button className="add-Data" type="submit">
              Submit
            </button>
          </form>
        )}
      </Formik>
    </div>
  );
}
