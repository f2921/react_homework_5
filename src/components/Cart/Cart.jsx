import "./Cart.scss";
import { BsStarFill } from "react-icons/bs";
import Formochka from "../Formochka/formochka";

export default function Cart({
  cartData,
  showDeleteModal,
  checkItemData,
  addcheckoutData,
}) {
  return (
    <div className="cart-container">
      <h1>Product Count {cartData.length}</h1>
      <div className="cart-section">
        <div className="cart-product-section">
          {cartData.length > 0 &&
            cartData.map((product) => {
              return (
                <div className="item-cart" key={product.id}>
                  <span
                    className="close"
                    onClick={() => {
                      showDeleteModal();
                      checkItemData(product);
                    }}
                  >
                    &times;
                  </span>
                  <div className="item__content">
                    <div className="item__content--img-block">
                      <img src={product.image} />
                    </div>
                    <div className="item__content--info-block">
                      <h3 className="item-title">{product.title}</h3>
                      <p className="item-price">{product.price} грн</p>
                      <p>Артикул: {product.id}</p>
                    </div>
                  </div>
                </div>
              );
            })}
        </div>
        {cartData.length > 0 && (
          <Formochka cartData={cartData} addcheckoutData={addcheckoutData} />
        )}
      </div>
    </div>
  );
}
