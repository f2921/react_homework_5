import { Route, Routes } from "react-router-dom";
import Header from "../Header/Header";
import ProductList from "../ProductList/ProductList";
import Cart from "../Cart/Cart";
import Favotites from "../Favorites/Favorites";

export default function Router({
  cartData,
  favData,
  products,
  addToFav,
  openAddToCartModal,
  checkItemData,
  deleteFavorite,
  showDeleteModal,
  addcheckoutData
}) {
  return (
    <Routes>
      <Route path="/" element={<Header  cartData={cartData} favData={favData}/>}>
        <Route
          index
          path="/"
          element={
            <ProductList
              products={products}
              showAddToCartModal={openAddToCartModal}
              checkItemData={checkItemData}
              addToFav={addToFav}
              favData={favData}
              deleteFavorite={deleteFavorite}
            />
          }
        />
        <Route
          path="/favorites"
          element={
            <Favotites favData={favData} deleteFavorite={deleteFavorite} showAddToCartModal={openAddToCartModal} checkItemData={checkItemData}/>
          }
        />
        <Route
          path="/cart"
          element={
            <Cart
              cartData={cartData}
              showDeleteModal={showDeleteModal}
              checkItemData={checkItemData}
              addcheckoutData={addcheckoutData}
            />
          }
        />
      </Route>
    </Routes>
  );
}

